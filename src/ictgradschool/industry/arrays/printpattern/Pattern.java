package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    private int num;
    private char symbol;

    public Pattern(int num, char symbol){
        this.num = num;
        this.symbol = symbol;
    }
    public String toString(){
        String toStr ="";
        for(int i=0;i<num;i++){
            toStr += symbol;
        }
        return toStr;
    }
    public void setNumberOfCharacters(int num){
        this.num = num;
    }

    public int getNumberOfCharacters(){
        return this.num;
    }
}
